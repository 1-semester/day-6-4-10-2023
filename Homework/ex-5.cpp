#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    int i = 1;
    double result = sqrt(2);

    while (i < n)
    {
        result = sqrt(2 + result);
        i++;
    }

    cout << "result = " << result << endl;
}
