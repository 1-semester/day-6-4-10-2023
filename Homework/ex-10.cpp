#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;
    double result = 0;
    double denominator = 0;

    while (i <= n)
    {
        denominator += sin(i * M_PI / 180);
        result += 1 / denominator;
        i++;
    }

    cout << "result = " << result << endl;
}
