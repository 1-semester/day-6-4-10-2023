#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;
    double result = 0;
    int numerator = 1;
    double denominator = 0;

    while (i <= n)
    {   
        numerator *= i;
        denominator += 1 / i;
        result += numerator / denominator;
        i++;
    }

    cout << "result = " << result << endl;
}
