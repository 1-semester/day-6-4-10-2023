#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    int i = 1;
    double result = 0;

    while (i <= n)
    {
        result += 1 / (2. * i + 1) / (2. * i + 1);
        i++;
    }

    cout << "result = " << result << endl;
}
