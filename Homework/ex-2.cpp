#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    int i = 1;
    double result = 1;

    while(i <= n) 
    {
        result *= 1 + 1. / i / i;
        i++;
    }

    cout << "result = " << result << endl;
}
