#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    int result = pow(2, n);
    cout << "result = " << result << endl;
}
