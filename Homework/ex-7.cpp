#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;
    double result = 0;
    int numerator = 1;

    while (i <= n)
    {
        result += numerator / i / (i + 1);
        numerator = -numerator;
        i++;
    }

    cout << "result = " << result << endl;
}
